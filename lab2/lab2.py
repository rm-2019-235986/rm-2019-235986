import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from math import *
from cmath import sqrt
from math import fabs
import random
import math
from numpy.ma import arange

ppy = random.randint(-10, 10)
pky = random.randint(-10, 10)

p_poczatkowy = (-10, ppy)
p_koncowy = (10, pky)

print("Punkt pocz膮tkowy: ", p_poczatkowy)
print("Punkt ko艅cowy: ", p_koncowy)

k0 = 1
KP = 0.02
koi = 10
d0 = 3.5
delta = 0.05
x_size = 10
y_size = 10

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = X + Y

px1 = random.randint(-10, 10)
py1 = random.randint(-10, 10)
px2 = random.randint(-10, 10)
py2 = random.randint(-10, 10)
px3 = random.randint(-10, 10)
py3 = random.randint(-10, 10)
px4 = random.randint(-10, 10)
py4 = random.randint(-10, 10)

#ilosc_przeszkod = 4
przeszkody = [(px1, py1),
              (px2, py2),
              (px3, py3),
              (px4, py4)]

sciezka = []

def Mnoz_Wector(v, r):
    vc = (v[0]*r, v[1]*r)
    return vc

def Dodaj_Wectory(v1, v2):
    v = (v1[0]+v2[0], v1[1]+v2[1])
    return v

def odleglosc(aa, bb):
    d = math.sqrt((aa[0] - bb[0])**2 + (aa[1] - bb[1])**2)
    #print(d)
    return d

def Licz_Wektor(p1, p2):
    v = (p2[0]-p1[0], p2[1]-p1[1])
    return v

def Dlugosc_Wektora(v):
    return np.sqrt(v[0]**2 + v[1]**2)

def Normalizuj(v):
    d = Dlugosc_Wektora(v)
    v = (v[0]/d, v[1]/d)
    return v

def Potencjal_przyciagajacy(wsp_robota, wsp_punktu):
    return 0.5*KP*(odleglosc(wsp_robota, wsp_punktu)**2)

def wartosc_sily_przyciagajacej(wsp_robota, wsp_koncowe): # jak w nazwie
    sila_przyciagajaca = KP * odleglosc(wsp_robota, wsp_koncowe)
    #print(sila_przyciagajaca)
    return sila_przyciagajaca

def Potencjal_odpychajacy(wsp_robota, wsp_punktu):
    if odleglosc(wsp_robota, wsp_punktu) > d0:
        return 0
    else:
        return 0.5*(K0*((1 / odleglosc(wsp_robota, wsp_punktu)) - (1 / d0))**2)

def wartosc_sil_odpychajacych(wsp_robota, przeszkody): # jak w nazwie
    odl = odleglosc(wsp_robota, przeszkody)
    if odl > d0:
        return 0
    else:
        so = koi * (((1 / odl) - (1 / d0)) * (1 / (odl**2)))
        #print(so)
        return so

def wartosc_sumy_sil_odpychajacych(wsp_robota, ilosc_przeszkod):
    suma = 0
    for przeszkoda in ilosc_przeszkod:
        suma += wartosc_sil_odpychajacych(wsp_robota, przeszkoda)
    #print(suma)
    return suma


def wartosc_sumy_sil(wsp_robota, wsp_punktu, ilosc_przeszkod):
    suma_sil = wartosc_sumy_sil_odpychajacych(wsp_robota, ilosc_przeszkod) + wartosc_sily_przyciagajacej(wsp_robota, wsp_punktu)
    #print(suma_sil)
    return suma_sil



def stworz_macierzXY():
    macierzXY = list()
    wiersz = list()
    for y in arange(-10.0, 10.0, delta):
        for x in arange(-10.0, 10.0, delta):
            wiersz.append((x, y))
        macierzXY.append(wiersz)
        wiersz = list()
    #print(macierzXY)
    return macierzXY

def stworz_macierz_sil(macierzXY, ilosc_przeszkod, wsp_robota):
    macierzsil = list()
    row = list()
    for line in macierzXY:
        for point in line:
            row.append(wartosc_sumy_sil(wsp_robota, point, ilosc_przeszkod))
        macierzsil.append(row)
        row = list()
    #print(macierzsil)
    return macierzsil

macierz_wspolrzednych = stworz_macierzXY()
Z = stworz_macierz_sil(macierz_wspolrzednych, przeszkody, p_koncowy)


pp = p_poczatkowy
ile = 800
while ile > 0:
    ile = ile-1
    if pp == p_koncowy:
        break

    pk = p_koncowy

    Vec_sp = wartosc_sily_przyciagajacej(pp,pk)
    Vecp = Licz_Wektor(pp, pk)
    Vecp= Normalizuj(Vecp)
    Vecp = Mnoz_Wector(Vecp, Vec_sp)
    V1 = (0,0)

    for przeszkoda in przeszkody:
        Vec_so = wartosc_sil_odpychajacych(pp, przeszkoda)
        Veco = Licz_Wektor(pp, przeszkoda)

        if Dlugosc_Wektora(Veco) > d0:
            Vec_so = 0

        Veco = Normalizuj(Veco)
        Veco = Mnoz_Wector(Veco, Vec_so)
        V1 = Dodaj_Wectory(V1, Veco)


    v_D = Normalizuj(Dodaj_Wectory(Vecp, Mnoz_Wector(V1, -1)))

    dx = round(v_D[0])
    dy = round(v_D[1])

    pp = (pp[0]+dx/10, pp[1]+dy/10)
    px = pp[0]
    py = pp[1]

    px = min(10, px)
    px = max(-10, px)
    py = min(10, py)
    py = max(-10, py)

    pp = (px, py)
    sciezka.append(pp)


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.Pastel1,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=0.5, vmin=0)

plt.plot(p_poczatkowy[0], p_poczatkowy[1], "or", color='green')
plt.plot(p_koncowy[0], p_koncowy[1], "or", color='blue')

for p in sciezka:
    plt.plot(p[0], p[1], "or", color='red', markersize=3)

for przeszkoda in przeszkody:
    plt.plot(przeszkoda[0], przeszkoda[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()

"""
1) Im wy偶szy wsp贸艂czynnik k0 tym wi臋ksza si艂a odpychaj膮ca.
2) Im wi臋ksza 艣rednia okr臋gu (d0) tym si艂y odpychaj膮ce maj膮 wi臋ksze pole oddzia艂ywania (zasi臋g).

"""
