from cmath import sqrt
from math import fabs
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random
import math
from numpy.ma import arange

ppy = random.randint(-10, 10)
pky = random.randint(-10, 10)

p_poczatkowy = (-10, ppy)
p_koncowy = (10, pky)

print("Punkt początkowy: ", p_poczatkowy)
print("Punkt końcowy: ", p_koncowy)

k0 = 1
kp = 0.001
koi = 1
d0 = 20
delta = 0.05
x_size = 10
y_size = 10

px1 = random.randint(-10, 10)
py1 = random.randint(-10, 10)
px2 = random.randint(-10, 10)
py2 = random.randint(-10, 10)
px3 = random.randint(-10, 10)
py3 = random.randint(-10, 10)
px4 = random.randint(-10, 10)
py4 = random.randint(-10, 10)

#ilosc_przeszkod = 4
przeszkody = [(px1, py1),
              (px2, py2),
              (px3, py3),
              (px4, py4)]

def odleglosc(pp, dp): # odleglosc miedzy punktami
    d = math.sqrt((pp[0] - dp[0])**2 + (pp[1] - dp[1])**2)
    #print(d)
    return d

def Potencjal_przyciagajacy(wsp_robota, wsp_punktu, KP):
    return 0.5*KP*(odleglosc(wsp_robota, wsp_punktu)**2)

def Potencjal_odpychajacy(wsp_robota, wsp_punktu, K0, D):
    if odleglosc(wsp_robota, wsp_punktu)>d0:
        return 0
    return 0.5*(K0*((1 / odleglosc(wsp_robota, wsp_punktu)) - (1 / D))**2)

def wartosc_sumy_sil(wsp_robota, wsp_punktu, ilosc_przeszkod, waga, D, K0i): # suma sil przy i odciagajacych
    suma_sil = wartosc_sumy_sil_odpychajacych(wsp_punktu, ilosc_przeszkod, waga, D) + wartosc_sily_przyciagajacej(wsp_punktu, wsp_robota, K0i)
    #print(suma_sil)
    return suma_sil

def wartosc_sily_przyciagajacej(wsp_robota, wsp_koncowe, KP): # jak w nazwie

    sila_przyciagajaca = KP * odleglosc(wsp_robota, wsp_koncowe)
    #print(sila_przyciagajaca)
    return sila_przyciagajaca

def wartosc_sil_odpychajacych(wsp_robota, przeszkody, K0i, D): # jak w nazwie
    odl = odleglosc(wsp_robota, przeszkody)
    if odl == 0:
        return 1
    elif odl <= D:
        so = -K0i * (((1 / odl) - (1 / D)) * (1 / (odl**2)))
        #print(so)
        return so
    else:
        return 0

def wartosc_sumy_sil_odpychajacych(wsp_robota, ilosc_przeszkod, K0i, D):
    suma = 0
    for przeszkoda in ilosc_przeszkod:
        suma += wartosc_sil_odpychajacych(wsp_robota, przeszkoda, K0i, D)
    #print(suma)
    return suma

def stworz_macierzXY():
    macierzXY = list()
    wiersz = list()
    for y in arange(-10.0, 10.0, delta):
        for x in arange(-10.0, 10.0, delta):
            wiersz.append((x, y))
        macierzXY.append(wiersz)
        wiersz = list()
    #print(macierzXY)
    return macierzXY

def stworz_macierz_sil(macierzXY, ilosc_przeszkod, wsp_robota, waga, K0, D):
    macierzsil = list()
    row = list()
    for line in macierzXY:
        for point in line:
            row.append(
                wartosc_sumy_sil(wsp_robota, point, ilosc_przeszkod, waga, D, K0))
        macierzsil.append(row)
        row = list()
    #print(macierzsil)
    return macierzsil

macierz_wspolrzednych = stworz_macierzXY()
Z = stworz_macierz_sil(macierz_wspolrzednych, przeszkody, p_koncowy, k0, kp, d0)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.Pastel1, origin='lower',
           extent=[-x_size, x_size, -y_size, y_size], vmax=0.01, vmin=0)

plt.plot(p_poczatkowy[0], p_poczatkowy[1], "or", color='green')
plt.plot(p_koncowy[0], p_koncowy[1], "or", color='blue')

for przeszkoda in przeszkody:
    plt.plot(przeszkoda[0], przeszkoda[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()

"""

1) Im wyższy współczynnik k0 tym większa siła odpychająca.
2) Im większa średnia okręgu (d0) tym siły odpychające mają większe pole oddziaływania (zasięg).

"""